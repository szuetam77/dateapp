package pl.java.DateApp.service.impl;

import org.springframework.stereotype.Service;
import pl.java.DateApp.service.DateOperatorService;

import java.util.Calendar;

@Service
public class DayOperatorService implements DateOperatorService {

    @Override
    public String returnDate(String unit){
        return String.valueOf(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
    }

    @Override
    public String key(){
        return "DAY_OF_WEEK";
    }
}