package pl.java.DateApp.service;

public interface DateOperatorService {
    String returnDate(String unit);
    String key();
}
