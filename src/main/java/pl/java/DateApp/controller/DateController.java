package pl.java.DateApp.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.java.DateApp.model.DateRequest;
import pl.java.DateApp.model.DateResponse;
import pl.java.DateApp.service.DateOperatorService;

import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/date")
public class DateController {


    private Map<String, DateOperatorService> operators;


    public DateController(Set<DateOperatorService> operatorServices) {
        this.operators = operatorServices.stream().collect(Collectors.toMap(DateOperatorService::key, Function.identity()));
    }

    @PostMapping("/date")
    public ResponseEntity<DateResponse> operation(@RequestBody DateRequest dateRequest) {
        DateResponse dateResponse = new DateResponse();
        DateOperatorService dateOperatorService = operators.get(dateRequest.getUnit());
        String result = dateOperatorService.returnDate(dateRequest.getUnit());
        dateResponse.setResult(result);
        return new ResponseEntity<>(dateResponse, HttpStatus.OK);
    }
}